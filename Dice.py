import random


class Dice:
    # dict of dice_size (d4,d6,d20) : count of dices
    dices: dict[int, int] = None

    modifiers: list[int] = None

    def __init__(self, size: int = 4, count: int = 1, dices: dict[int, int] = None, modifiers: list[int] = None):
        """
        init the dice
        size: the size of the dice (d4/6/12)
        count: count of dices
        dices: dict of dice_size : count of dice
        """
        if not dices is None:  # if dices is set
            self.dices = {k: v[:] for k, v in dices.items()}  # deep copy
        else:  # if dices is not set
            self.dices = {}  # init dict
            self.dices[size] = count  # copy dice value
        # print(self.dices)
        self.modifiers = modifiers[:] if modifiers != None else []

    def roll(self):
        """
        roll the dice/s
        """
        res = []  # init result array
        for s, c in self.dices.items():  # run on dices
            # print(str(s)+":", c)
            for _ in range(c):  # run on dice count
                res.append(random.randint(1, s))  # roll the dice

        return res

    def __add__(self, other):
        """ add different dices together """
        if type(other) == type(self):  # if same type
            d = {}  # new dict
            for k, v in other.dices.items():  # run on other's dices
                if k in d:  # if key (dice-size) in dict
                    d[k] += v  # add dice count
                else:
                    d[k] = v  # add new key = dice count
            for k, v in self.dices.items():  # same as above, but for self's dices
                if k in d:
                    d[k] += v
                else:
                    d[k] = v
            return Dice(d)  # return new dice
        if type(other) in [int, float]:  # if other is number
            return other + self.__int__(self)  # add int of self

    def __int__(self):
        """ converts the dice to int by returning the sum of the roll list """
        return sum(self.roll())


# d = Dice(8, 5)

# print(d.roll(), int())
