from basic_lib import *

from Dice import *


class ItemTypes(myEnum):
    def __eq__(self, other):
        return self.name == other.name and self.value == other.value

    def of_type(self, type):
        return len(type.value) < len(self.value) and all([type.value[i] == self.value[i] for i in range(len(type.value))])

    def any_of_type_self(self, *types):
        return [t for t in types if t.of_type(self)]

    Armor = (1,)

    LightArmor = (1, 0,)

    Padded_Armor = (1, 0, 0)
    Leather_Armor = (1, 0, 1)
    Studded_Leather_Armor = (1, 0, 2)

    MediumArmor = (1, 1,)

    Hide_Armor = (1, 1, 0)
    Chain_Shirt_Armor = (1, 1, 1)
    Scale_Male_Armor = (1, 1, 2)
    Brestplate_Armor = (1, 1, 3)
    Half_Plate_Armor = (1, 1, 4)

    HeavyArmor = (1, 2,)

    Ring_Mail_Armor = (1, 2, 0)
    Chain_Mail_Armor = (1, 2, 1)
    Splint_Armor = (1, 2, 2)
    Plate_Armor = (1, 2, 3)

    Shield = (1, 3,)

    Weapons = (2,)

    Simple_Weapons = (2, 0)

    Simple_Melee_Weapons = (2, 0, 0)

    Club = (2, 0, 0, 1)
    Dagger = (2, 0, 0, 2)
    Greatclub = (2, 0, 0, 3)
    Handaxe = (2, 0, 0, 4)
    javelin = (2, 0, 0, 5)
    Light = (2, 0, 0, 6)
    Mace = (2, 0, 0, 7)
    Quarterstaff = (2, 0, 0, 8)
    Sickle = (2, 0, 0, 9)
    Spear = (2, 0, 0, 10)
    Unarmed = (2, 0, 0, 11)

    Simple_Ranged_Weapons = (2, 0, 1)

    Crossbow = (2, 0, 1, 0)
    Dart = (2, 0, 1, 1)
    Shortbow = (2, 0, 1, 2)
    Sling = (2, 0, 1, 3)

    Martial_Weapons = (2, 1,)

    Martial_Melee_Weapons = (2, 1, 0)

    Battleaxe = (2, 1, 0, 0)
    Flail = (2, 1, 0, 1)
    Glaive = (2, 1, 0, 2)
    Greataxe = (2, 1, 0, 3)
    Greatsword = (2, 1, 0, 4)
    Halberd = (2, 1, 0, 5)
    Lance = (2, 1, 0, 6)
    Longsword = (2, 1, 0, 7)
    Maul = (2, 1, 0, 8)
    Morningstar = (2, 1, 0, 9)
    Pike = (2, 1, 0, 10)
    Rapier = (2, 1, 0, 11)
    Scimitar = (2, 1, 0, 12)
    Shortsword = (2, 1, 0, 13)
    Trident = (2, 1, 0, 14)
    War_pick = (2, 1, 0, 15)
    Warhammer = (2, 1, 0, 16)
    Whip = (2, 1, 0, 17)

    Martial_Ranged_Weapons = (2, 1, 1,)

    Blowgun = (2, 1, 1, 0)
    Crossbow_hand = (2, 1, 1, 1)
    Crossbow_heavy = (2, 1, 1, 2)
    Longbow = (2, 1, 1, 3)
    Net = (2, 1, 1, 4)

    Adventuring_Gear = (3,)

    Druidic_focus = (3, 0)

    Sprig_of_mistletoe = (3, 0, 0)
    Totem = (3, 0, 1)
    Wooden_staff = (3, 0, 2)
    Yew_wand = (3, 0, 3)

    clothes = (3, 1)

    common = (3, 1, 0)
    costume = (3, 1, 1)
    fine = (3, 1, 2)
    travelers = (3, 1, 3)

    Arcane_focus = (3, 2)

    Crystal = (3, 2, 0)
    Orb = (3, 2, 1)
    Rod = (3, 2, 2)
    Staff = (3, 2, 3)
    Wand = (3, 2, 4)

    Ammunition = (3, 3)

    Arrows_20 = (3, 3, 0)
    Blowgun_needles_50 = (3, 3, 1)
    Crossbow_bolts_20 = (3, 3, 2)
    Sling_bullets_20 = (3, 3, 3)

    Abacus = (3, 4)
    Acid_vial = (3, 5)
    Alchemists_fire_flask = (3, 6)
    Antitoxin_vial = (3, 7)
    Backpack = (3, 8)
    Ball_bearings_bag_of_1000 = (3, 9)
    Barrel = (3, 10)
    Basket = (3, 11)
    Bedroll = (3, 12)
    Bell = (3, 13)
    Blanket = (3, 14)
    Block_and_tackle = (3, 15)
    Book = (3, 16)
    Bottle_glass = (3, 17)
    Bucket = (3, 18)
    Caltrops_bag_of_20 = (3, 19)
    Candle = (3, 20)
    Case_crossbow_bolt = (3, 21)
    Case_map_or_scroll = (3, 22)
    Chain_10_feet = (3, 23)
    Chalk_1_piece = (3, 24)
    Chest = (3, 25)
    Climbers_kit = (3, 26)
    Component_pouch = (3, 27)
    Crowbar = (3, 28)
    Fishing_tackle = (3, 29)
    Flask_or_tankard = (3, 30)
    Grappling = (3, 31)
    Hammer = (3, 32)
    Hammer_sledge = (3, 33)
    Healers_kit = (3, 34)

    Holy_symbol = (3, 35)

    Amulet = (3, 35, 0)
    Emblem = (3, 35, 1)
    Reliquary = (3, 35, 2)

    Holy_water_flask = (3, 36)
    Hourglass = (3, 37)
    Hunting = (3, 38)
    Ink_1_ounce_bottle = (3, 39)
    Ink_pen = (3, 40)
    Jug_or_pitcher = (3, 41)
    Ladder_10_foot = (3, 42)
    Lamp = (3, 43)
    Lantern_bullseye = (3, 44)
    Lantern_hooded = (3, 45)
    Lock = (3, 46)
    Magnifying_glass = (3, 47)
    Manacles = (3, 48)
    Mess_kit = (3, 49)
    Mirror_steel = (3, 50)
    Oil_flask = (3, 51)
    Paper_one_sheet = (3, 52)
    Parchment_one_sheet = (3, 53)
    Perfume_vial = (3, 54)
    Pick_miners = (3, 55)
    Piton = (3, 56)
    Poison_basic_vial_ = (3, 57)
    Pole_10_foot = (3, 58)
    Pot_iron = (3, 59)
    Potion_of_healing = (3, 60)
    Pouch = (3, 61)
    Quiver = (3, 62)
    Ram_portable = (3, 63)
    Rations_1_day = (3, 64)
    Robes = (3, 65)
    Rope_hempen_50_feet = (3, 66)
    Rope_silk_50_feet = (3, 67)
    Sack = (3, 68)
    Scale_merchants = (3, 69)
    Sealing_wax = (3, 70)
    Shovel = (3, 71)
    Signal_whistle = (3, 72)
    Signet_ring = (3, 73)
    Soap = (3, 74)
    Spellbook = (3, 75)
    Spikes_iron_10 = (3, 76)
    Spyglass = (3, 77)
    Tent_two_person = (3, 78)
    Tinderbox = (3, 79)
    Torch = (3, 80)
    Vial = (3, 81)
    Waterskin = (3, 82)
    Whetstone = (3, 83)

    Tools = (4,)

    Disguise_kit = (4, 0)
    Forgery_kit = (4, 1)
    Herbalism_kit = (4, 2)
    Navigators_tools = (4, 3)
    Poisoners_kit = (4, 4)
    Thieves_tools = (4, 5)

    Artisans_tools = (4, 6)

    Alchemists_supplies = (4, 6, 0)
    Brewers_supplies = (4, 6, 1)
    Calligraphers_supplies = (4, 6, 2)
    Carpenters_tools = (4, 6, 3)
    Cartographers_tools = (4, 6, 4)
    Cobblers_tools = (4, 6, 5)
    Cooks_utensils = (4, 6, 6)
    Glassblowers_tools = (4, 6, 7)
    Jewelers_tools = (4, 6, 8)
    Leatherworkers_tools = (4, 6, 9)
    Masons_tools = (4, 6, 10)
    Painters_supplies = (4, 6, 11)
    Potters_tools = (4, 6, 12)
    Smiths_tools = (4, 6, 13)
    Tinkers_tools = (4, 6, 14)
    Weavers_tools = (4, 6, 15)
    Woodcarvers_tools = (4, 6, 16)

    Gaming_set = (4, 7)

    Dice_set = (4, 7, 0)
    Dragonchess_set = (4, 7, 1)
    Playing_card_set = (4, 7, 2)
    Three_Dragon_Ante_set = (4, 7, 3)

    Musical_instrument = (4, 8)

    Bagpipes = (4, 8, 0)
    Drum = (4, 8, 1)
    Dulcimer = (4, 8, 2)
    Flute = (4, 8, 3)
    Lute = (4, 8, 4)
    Lyre = (4, 8, 5)
    Horn = (4, 8, 6)
    Pan = (4, 8, 7)
    Shawm = (4, 8, 8)
    Viol = (4, 8, 9)

    none = (0,)


class weight_units(myEnum):
    lb = 1
    kg = 2

    none = 0


class _weight:
    w: float = 0
    u: weight_units = weight_units.none

    def __init__(self, w: float = 0, u: weight_units = None):
        self.w = w
        self.u = u


class Currency(myEnum):
    CP = 1
    SP = 10
    EP = 50
    GP = 100
    PP = 1000

    none = 0


class _currency:
    curr_dict: dict[Currency, int] = []

    def __init__(self, curr_dict: dict[Currency, int] = []):
        self.curr_dict = {k: v for k, v in curr_dict.items()}  # deep copy

    def has_money(self, curr: Currency = Currency.none, amount: int = 0):
        return ((b := (c := self.curr_dict[curr]) >= amount), 0 if b else amount - c)

    def use_money(self, curr: Currency = Currency.none, amount: int = 0):
        if not (c := self.has_money(curr, amount))[0]:
            return (False, c[1])
        self.curr_dict[curr] = (r := self.curr_dict[curr] - amount)
        return (True, r)

    def get_money(self, curr: Currency = Currency.none, amount: int = 0, curr_dict: dict[Currency, int] = []):
        self.curr_dict[curr] += amount
        for cu, am in curr_dict.items():
            self.curr_dict[cu] += am


class Item:
    name: str = ""
    short_description: str = ""
    long_description: str = ""
    type: ItemTypes = ItemTypes.none
    weight: _weight = None
    cost: _currency = None
    count: int = 0

    def __init__(self,
                 name: str = "",
                 short_description: str = "",
                 long_description: str = "",
                 type: ItemTypes = ItemTypes.none,
                 weight: _weight = None,
                 cost: _currency = None,
                 count: int = 0):
        self.name = name
        self.short_description = short_description
        self.long_description = long_description
        self.type = type
        self.weight = weight
        self.cost = cost
        self.count = count

    def __eq__(self, other):
        return other.name == self.name and other.type == self.type


class MagicItem(Item):
    requires_attunement: bool = False
    is_attuned: bool = False
    magic_effect: str = ""
    _magic_action: function = None


class damage_types(myEnum):
    Acid = 1
    Bludgeoning = 2
    Cold = 3
    Fire = 4
    Force = 5
    Lightning = 6
    Necrotic = 7
    Piercing = 8
    Poison = 9
    Psychic = 10
    Radiant = 11
    Slashing = 12
    Thinder = 13

    none = 0


class _weapon_properties(myEnum):
    Ammunition = {
        "name": "Ammunition",
        "description": "You can use a weapon that has the ammunition property to make a ranged attack only if you have ammunition to fire from the weapon. Each time you attack with the weapon, you expend one piece of ammunition. Drawing the ammunition from a quiver, case, or other container is part of the attack. At the end of the battle, you can recover half your expended ammunition by taking a minute to search the battlefield.",
        # len(t:=ItemTypes.Ammunition.any_of_type(Inventory.all_items)) > 0 and w(the weapon).Ammunition_type in t
        "condition": lambda p, w: False,
        # Inventory.all_items.find(w.Ammunition_type).count -= 1
        "use": lambda p, w: None
    },
    Finesse = {
        "name": "Finesse",
        "description": "When making an attack with a finesse weapon, you use your choice of your Strength or Dexterity modifier for the attack and damage rolls. You must use the same modifier for both rolls.Heavy. Small creatures have disadvantage on attack rolls with heavy weapons. A heavy weapon’s size and bulk make it too large for a Small creature to use effectively.",
        "roll": lambda p, w: None
    },
    Light = {
        "name": "Light",
        "description": "A light weapon is small and easy to handle, making it ideal for use when fighting with two weapons. See the rules for two-weapon fighting in chapter 9.",
    },
    Loading = {
        "name": "Loading",
        "description": "Because of the time required to load this weapon, you can fire only one piece of ammunition from it when you use an action, bonus action, or reaction to fire it, regardless of the number of attacks you can normally make.",
    },
    Range = {
        "name": "Range",
        "description": "A weapon that can be used to make a ranged attack has a range shown in parentheses after the ammunition or thrown property. The range lists two numbers. The first is the weapon’s normal range in feet, and the second indicates the weapon’s maximum range. When attacking a target beyond normal range, you have disadvantage on the attack roll. You can’t attack a target beyond the weapon’s long range.",
    },
    Reach = {
        "name": "Reach",
        "description": "This weapon adds 5 feet to your reach when you attack with it.Special. A weapon with the special property has unusual rules governing its use, explained in the weapon’s description (see \"Special Weapons\" later in this section).",
    },
    Thrown = {
        "name": "Thrown",
        "description": "If a weapon has the thrown property, you can throw the weapon to make a ranged attack. If the weapon is a melee weapon, you use the same ability modifier for that attack roll and damage roll that you would use for a melee attack with the weapon. For example, if you throw a handaxe, you use your Strength, but if you throw a dagger, you can use either your Strength or your Dexterity, since the dagger has the finesse property.",
    },
    Two_Handed = {
        "name": "Two-Handed",
        "description": "This weapon requires two hands to use.",
        "use_condition": lambda p, w: False  # only holding this weapon
    },
    Versatile = {
        "name": "Versatile",
        "description": "This weapon can be used with one or two hands. A damage value in parentheses appears with the property—the damage when the weapon is used with two hands to make a melee attack.",
        "condition": lambda p, w: False  # only holding this weapon
    }
    none = 0


class WeaponProperties:
    name: str = ""
    description: str = ""
    condition: function = None
    roll: function = None
    use: function = None
    use_condition: function = None

    range: tuple[int, int] = None

    def __init__(self, data_dict={},
                 name: str = "",
                 description: str = "",
                 condition: function = None,
                 roll: function = None,
                 use: function = None,
                 use_condition: function = None,
                 range: tuple[int, int] = None,
                 ):
        if "name" in data_dict:
            self.name = data_dict["name"]
        else:
            self.name = name

        if "condition" in data_dict:
            self.condition = data_dict["condition"]
        else:
            self.description = description

        if "condition" in data_dict:
            self.condition = data_dict["condition"]
        else:
            self.condition = condition

        if "roll" in data_dict:
            self.roll = data_dict["roll"]
        else:
            self.roll = roll

        if "use" in data_dict:
            self.use = data_dict["use"]
        else:
            self.use = use

        if "use_condition" in data_dict:
            self.use_condition = data_dict["use_condition"]
        else:
            self.use_condition = use_condition

        self.range = range


class Weapon(Item):
    def is_ranged(self): return self.type.of_type(
        ItemTypes.Simple_Ranged_Weapons) or self.type.of_type(ItemTypes.Martial_Ranged_Weapons)

    def is_melee(self): return self.type.of_type(
        ItemTypes.Martial_Melee_Weapons) or self.type.of_type(ItemTypes.Simple_Melee_Weapons)

    damage: Dice = None
    dmg_type: damage_types = damage_types.none
    properties: WeaponProperties = None
    ammo_type: ItemTypes = ItemTypes.none


class Armor:

    AC: int = 0


class CharacterInventory:

    Backpack: list[Item] = []
    Weapons: list[Weapon] = []
    Armor: list[Armor] = []
    Currency: _currency = []

    def __init__(self):
        pass

    def add_item(self, item: Item):
        for _in, it in enumerate(self.Backpack):
            if it == item:
                self.Backpack[_in].count = (
                    t := (self.Backpack[_in].count + 1))
                return t
        self.Backpack.append(item)
        return 1

    def has_item(self, item: Item):
        return ((b := item in self.Backpack), -1 if not b else self.Backpack.index(item))

    def remove_item(self, item: Item, count: int = 1):
        if not (i := self.has_item(item))[0]:
            return 0
        it = self.Backpack[i[1]]
        c = it.count - count
        if it.count >= 1:
            it.count = c
            return count
        else:
            del self.Backpack[i[1]]
            return c + count  # c = it.count - count; c + count = it.count

    def get_ac(self, p):
        if len(self.Armor) == 0:
            ...
        else:
            ...
