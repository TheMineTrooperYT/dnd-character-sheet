#!/usr/bin/pytho"",

from print_lib import *
from enum import Enum
from Dice import *


class id:
    ids: dict[int, int] = {}

    def __new__(cls, i: int) -> tuple[int]:
        if i in cls.ids:
            cls.ids[i] = (t := (cls.ids[i] + 1))
            return (i, t)
        cls.ids[i] = (t := 0)
        return (i, t)


class _weight:
    val: int = 0
    unit: str = "lb"

    def __init__(self, val, unit=None):
        self.val = val
        self.unit = unit if not unit is None else self.unit


class _currency_unit(Enum):
    CP = 1
    SP = 10
    EP = 50
    GP = 100
    PP = 1000

    none = None


class _currency:
    val: int = 0
    unit: _currency_unit = _currency_unit.GP

    def __init__(self, val, unit=None):
        self.val = val
        self.unit = unit if not unit is None else self.unit

    def to_unit(self, unit: _currency_unit):
        return self.val / self.unit.value * unit.value


class _item:
    name: str = ""
    description: str = ""
    weight: _weight = None
    cost: _currency = None


class armor(Enum):
    LightArmor = 0

    MediumArmor = 1

    HeavyArmor = 2

    _shield = 3

    none = None


class _armor(_item):
    ac: int = 0
    strength: int = 0
    stealth: bool = False
    type: armor = armor.none

    def __init__(self, name, description, weight, cost, ac, strength, stealth, type):
        self.name, self.description, self.weight, self.cost, self.ac, self.strength, self.stealth, self.type = name, description, weight, cost, ac, strength, stealth, type


armor.Padded_Armor = _armor("Padded Armor", "", _weight(
    0), _currency(0), 10, 0, False, armor.LightArmor)
armor.Leather_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.LightArmor)
armor.Studded_Leather_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.LightArmor)

armor.Hide_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.MediumArmor)
armor.Chain_Shirt_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.MediumArmor)
armor.Scale_Male_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.MediumArmor)
armor.Brestplate_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.MediumArmor)
armor.Half_Plate_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.MediumArmor)

armor.Ring_Mail_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.HeavyArmor)
armor.Chain_Mail_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.HeavyArmor)
armor.Splint_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.HeavyArmor)
armor.Plate_Armor = _armor("", "", _weight(
    0), _currency(0), 0, 0, False, armor.HeavyArmor)

armor.Shield = _armor("", "", _weight(0), _currency(0),
                      2, 0, False, armor._shield)


class _weapon(_item):
    ...


class weapons(Enum):
    Simple_Weapons = (2, 0)

    Simple_Melee_Weapons = (2, 0, 0)

    Club = (2, 0, 0, 1)
    Dagger = (2, 0, 0, 2)
    Greatclub = (2, 0, 0, 3)
    Handaxe = (2, 0, 0, 4)
    javelin = (2, 0, 0, 5)
    Light = (2, 0, 0, 6)
    Mace = (2, 0, 0, 7)
    Quarterstaff = (2, 0, 0, 8)
    Sickle = (2, 0, 0, 9)
    Spear = (2, 0, 0, 10)

    Simple_Ranged_Weapons = (2, 0, 1)

    Crossbow = (2, 0, 1, 0)
    Dart = (2, 0, 1, 1)
    Shortbow = (2, 0, 1, 2)
    Sling = (2, 0, 1, 3)

    Martial_Weapons = (2, 1,)

    Martial_Melee_Weapons = (2, 1, 0)

    Battleaxe = (2, 1, 0, 0)
    Flail = (2, 1, 0, 1)
    Glaive = (2, 1, 0, 2)
    Greataxe = (2, 1, 0, 3)
    Greatsword = (2, 1, 0, 4)
    Halberd = (2, 1, 0, 5)
    Lance = (2, 1, 0, 6)
    Longsword = (2, 1, 0, 7)
    Maul = (2, 1, 0, 8)
    Morningstar = (2, 1, 0, 9)
    Pike = (2, 1, 0, 10)
    Rapier = (2, 1, 0, 11)
    Scimitar = (2, 1, 0, 12)
    Shortsword = (2, 1, 0, 13)
    Trident = (2, 1, 0, 14)
    War_pick = (2, 1, 0, 15)
    Warhammer = (2, 1, 0, 16)
    Whip = (2, 1, 0, 17)

    Martial_Ranged_Weapons = (2, 1, 1,)

    Blowgun = (2, 1, 1, 0)
    Crossbow_hand = (2, 1, 1, 1)
    Crossbow_heavy = (2, 1, 1, 2)
    Longbow = (2, 1, 1, 3)
    Net = (2, 1, 1, 4)


class items(Enum):
    Adventuring_Gear = (3,)

    Druidic_focus = (3, 0)

    Sprig_of_mistletoe = (3, 0, 0)
    Totem = (3, 0, 1)
    Wooden_staff = (3, 0, 2)
    Yew_wand = (3, 0, 3)

    clothes = (3, 1)

    common = (3, 1, 0)
    costume = (3, 1, 1)
    fine = (3, 1, 2)
    travelers = (3, 1, 3)

    Arcane_focus = (3, 2)

    Crystal = (3, 2, 0)
    Orb = (3, 2, 1)
    Rod = (3, 2, 2)
    Staff = (3, 2, 3)
    Wand = (3, 2, 4)

    Ammunition = (3, 3)

    Arrows_20 = (3, 3, 0)
    Blowgun_needles_50 = (3, 3, 1)
    Crossbow_bolts_20 = (3, 3, 2)
    Sling_bullets_20 = (3, 3, 3)

    Abacus = (3, 4)
    Acid_vial = (3, 5)
    Alchemists_fire_flask = (3, 6)
    Antitoxin_vial = (3, 7)
    Backpack = (3, 8)
    Ball_bearings_bag_of_1000 = (3, 9)
    Barrel = (3, 10)
    Basket = (3, 11)
    Bedroll = (3, 12)
    Bell = (3, 13)
    Blanket = (3, 14)
    Block_and_tackle = (3, 15)
    Book = (3, 16)
    Bottle_glass = (3, 17)
    Bucket = (3, 18)
    Caltrops_bag_of_20 = (3, 19)
    Candle = (3, 20)
    Case_crossbow_bolt = (3, 21)
    Case_map_or_scroll = (3, 22)
    Chain_10_feet = (3, 23)
    Chalk_1_piece = (3, 24)
    Chest = (3, 25)
    Climbers_kit = (3, 26)
    Component_pouch = (3, 27)
    Crowbar = (3, 28)
    Fishing_tackle = (3, 29)
    Flask_or_tankard = (3, 30)
    Grappling = (3, 31)
    Hammer = (3, 32)
    Hammer_sledge = (3, 33)
    Healers_kit = (3, 34)

    Holy_symbol = (3, 35)

    Amulet = (3, 35, 0)
    Emblem = (3, 35, 1)
    Reliquary = (3, 35, 2)

    Holy_water_flask = (3, 36)
    Hourglass = (3, 37)
    Hunting = (3, 38)
    Ink_1_ounce_bottle = (3, 39)
    Ink_pen = (3, 40)
    Jug_or_pitcher = (3, 41)
    Ladder_10_foot = (3, 42)
    Lamp = (3, 43)
    Lantern_bullseye = (3, 44)
    Lantern_hooded = (3, 45)
    Lock = (3, 46)
    Magnifying_glass = (3, 47)
    Manacles = (3, 48)
    Mess_kit = (3, 49)
    Mirror_steel = (3, 50)
    Oil_flask = (3, 51)
    Paper_one_sheet = (3, 52)
    Parchment_one_sheet = (3, 53)
    Perfume_vial = (3, 54)
    Pick_miners = (3, 55)
    Piton = (3, 56)
    Poison_basic_vial_ = (3, 57)
    Pole_10_foot = (3, 58)
    Pot_iron = (3, 59)
    Potion_of_healing = (3, 60)
    Pouch = (3, 61)
    Quiver = (3, 62)
    Ram_portable = (3, 63)
    Rations_1_day = (3, 64)
    Robes = (3, 65)
    Rope_hempen_50_feet = (3, 66)
    Rope_silk_50_feet = (3, 67)
    Sack = (3, 68)
    Scale_merchants = (3, 69)
    Sealing_wax = (3, 70)
    Shovel = (3, 71)
    Signal_whistle = (3, 72)
    Signet_ring = (3, 73)
    Soap = (3, 74)
    Spellbook = (3, 75)
    Spikes_iron_10 = (3, 76)
    Spyglass = (3, 77)
    Tent_two_person = (3, 78)
    Tinderbox = (3, 79)
    Torch = (3, 80)
    Vial = (3, 81)
    Waterskin = (3, 82)
    Whetstone = (3, 83)


class tools(Enum):
    Tools = (4,)

    Disguise_kit = (4, 0)
    Forgery_kit = (4, 1)
    Herbalism_kit = (4, 2)
    Navigators_tools = (4, 3)
    Poisoners_kit = (4, 4)
    Thieves_tools = (4, 5)

    Artisans_tools = (4, 6)

    Alchemists_supplies = (4, 6, 0)
    Brewers_supplies = (4, 6, 1)
    Calligraphers_supplies = (4, 6, 2)
    Carpenters_tools = (4, 6, 3)
    Cartographers_tools = (4, 6, 4)
    Cobblers_tools = (4, 6, 5)
    Cooks_utensils = (4, 6, 6)
    Glassblowers_tools = (4, 6, 7)
    Jewelers_tools = (4, 6, 8)
    Leatherworkers_tools = (4, 6, 9)
    Masons_tools = (4, 6, 10)
    Painters_supplies = (4, 6, 11)
    Potters_tools = (4, 6, 12)
    Smiths_tools = (4, 6, 13)
    Tinkers_tools = (4, 6, 14)
    Weavers_tools = (4, 6, 15)
    Woodcarvers_tools = (4, 6, 16)

    Gaming_set = (4, 7)

    Dice_set = (4, 7, 0)
    Dragonchess_set = (4, 7, 1)
    Playing_card_set = (4, 7, 2)
    Three_Dragon_Ante_set = (4, 7, 3)

    Musical_instrument = (4, 8)

    Bagpipes = (4, 8, 0)
    Drum = (4, 8, 1)
    Dulcimer = (4, 8, 2)
    Flute = (4, 8, 3)
    Lute = (4, 8, 4)
    Lyre = (4, 8, 5)
    Horn = (4, 8, 6)
    Pan = (4, 8, 7)
    Shawm = (4, 8, 8)
    Viol = (4, 8, 9)

    none = (0,)


class abilities(Enum):
    str = STR = "Strength"
    dex = DEX = "Dexterity"
    con = CON = "Constitution"
    int = INT = "Intelligance"
    wis = WIS = "Wisdom"
    cha = CHA = "Charisma"

    none = "none"


class skills(Enum):
    Acrobatics = abilities.dex
    Animal = abilities.wis
    Arcana = abilities.int
    Athletics = abilities.dex
    Deception = abilities.cha
    History = abilities.int
    Insight = abilities.wis
    Intimidation = abilities.cha
    Investigation = abilities.int
    Medicine = abilities.wis
    Nature = abilities.int
    Perception = abilities.wis
    Performance = abilities.cha
    Persuasion = abilities.cha
    Religion = abilities.int
    Sleight = abilities.dex
    Stealth = abilities.dex
    Survival = abilities.wis

    none = None


class sizes(Enum):
    Small = id(26)
    Medium = id(26)
    Large = id(26)

    none = None


class trait:
    name: str = ""
    description: str = ""

    ASI = None
    prof = None

    def __init__(self, name, description, asi=None, prof=None):
        self.name, self.description, self.ASI, self.prof = name, description, asi, prof


class _race:
    name: str = ""
    description: str = ""

    size: sizes = sizes.none
    speed: int = 0
    traits: list[trait] = []

    def __init__(self, *args):
        self.name, self.description, self.size, self.speed, self.traits = args


class races(Enum):
    hill_dwarf = _race('hill dwarf', "", sizes.Medium, 25, [trait("darkvision", ""), trait("dwarven resilience", ""), trait("Ability Score Increase", "", asi={
        abilities.con: 2, abilities.wis: 1})])

    none = None


class _class:
    name: str = ""
    description: str = ""
    primary_ability: abilities = abilities.none
    hit_dice: Dice = ""
    saving_throw_profs: list[abilities] = []
    armor_weapon_profs = []
    traits: dict[int, list[trait]] = []
    level = 0

    def __init__(self, *args):
        if len(args) == 1 and type(args[0]) == dict:
            args = args[0]
            self.name = args['name'] if 'name' in args else self.name
            self.description = args['description'] if 'description' in args else self.description
            self.primary_ability = args['primary_ability'] if 'primary_ability' in args else self.primary_ability
            self.hit_dice = args['hit_dice'] if 'hit_dice' in args else self.hit_dice
            self.saving_throw_profs = args['saving_throw_profs'] if 'saving_throw_profs' in args else self.saving_throw_profs
            self.armor_weapon_profs = args['armor_weapon_profs'] if 'armor_weapon_profs' in args else self.armor_weapon_profs
            self.traits = args['traits'] if 'traits' in args else self.traits
            self.level = args['level'] if 'level' in args else self.level
        elif type(args) in [list, tuple]:
            self.name, self.description, self.primary_ability, self.hit_dice, self.saving_throw_profs, self.armor_weapon_profs, self.traits, self.level = args


class classes(Enum):
    Barbarian = {
        "name": "Barbarian",
        "description": "A fierce warrior of primilive background who can enter a battle rage",
        "primary_ability": abilities.STR,
        "hit_dice": Dice(12),
        "saving_throw_profs": [abilities.STR, abilities.CON],
        "armor_weapon_profs": [armor.LightArmor, armor.MediumArmor, armor.Shield, weapons.Simple_Weapons, weapons.Martial_Weapons],
        "traits": {"1": [trait("ASI", "ASI", asi={abilities.dex: 1})], "2": [trait("ASI", "ASI", asi={abilities.str: 2})]},
        "level": 1
    }
    Bard = {
        "name": "Bard",
        'description': "An inspiring magician whose power echoes the music of creation",
        "primary_ability": abilities.CHA,
        "hit_dice": Dice(8),
        "saving_throw_profs": [abilities.DEX, abilities.CHA],
        "armor_weapon_profs": [armor.LightArmor, weapons.Simple_Weapons, weapons.Crossbow_hand, weapons.Longsword, weapons.Rapier, weapons.Shortsword],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Cleric = {
        "name": "Cleric",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Druid = {
        "name": "Druid",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Fighter = {
        "name": "Fighter",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Monk = {
        "name": "Monk",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Paladin = {
        "name": "Paladin",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Ranger = {
        "name": "Ranger",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Rogue = {
        "name": "Rogue",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Sorcerer = {
        "name": "Sorcerer",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Warlock = {
        "name": "Warlock",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }
    Wizard = {
        "name": "Wizard",
        'description': "",
        "primary_ability": abilities.none,
        "hit_dice": Dice(10),
        "saving_throw_profs": [abilities.none, abilities.none],
        "armor_weapon_profs": [],
        "traits": {"1": [], "2": []},
        "level": 1
    }


def mod(ab): return (ab-10)//2


def clear_dup(l): return list(dict.fromkeys(l))


def copy_dict(d): return {key: value for key, value in d.items()}


class Player:

    Name: str = ""

    Race: races = races.none

    Gender: str = ""

    _classes: list[_class] = []

    @property
    def Classes(self) -> list[_class]:
        return self._classes

    @Classes.setter
    def Classes(self, o):
        log(o, " ", type(o))
        if type(o) == classes:
            o = o.value
        if type(o) is dict:
            if all(t := [k in o for k in [
                "name",
                'description',
                "primary_ability",
                "hit_dice",
                "saving_throw_profs",
                "armor_weapon_profs",
                "traits",
                "level",
            ]]):
                found = False
                for i, c in enumerate(self._classes):
                    if c.name == o['name']:
                        log("Class ", c.name,
                            " already in Player's Classes, overwriting.")
                        self._classes[i] = _class(o)
                        found = True
                        break
                if not found:
                    log("Adding new ", o['name'], " class.")
                    self._classes.append(_class(o))
            else:
                elog("Received Invalid Class dict.")
            log(t)
        elif type(o) is _class:
            found = False
            for i, c in enumerate(self._classes):
                if c.name == o.name:
                    log("Class ", c.name,
                        " already in Player's Classes, overwriting.")
                    self._classes[i] = c
                    found = True
                    break
            if not found:
                log("Adding new ", o['name'], " class.")
                self._classes.append(o)

        elif type(o) in [list, tuple]:
            for rem_c in o:
                for i, c in enumerate(self._classes):
                    if c.name == (rem_c.name if type(rem_c) == _class else rem_c if type(rem_c) == str else rem_c["name"] if type(rem_c) == dict and "name" in rem_c else None):
                        elog("Found Class ", c.name, ", Removing it.")
                        self._classes.remove(c)

    def level_up_class(self, cls, lvl=1):
        for i, c in enumerate(self._classes):
            # log((cls.name if type(cls) in [_class, classes] else cls if type(
            #     cls) == str else cls["name"] if type(cls) == dict and "name" in cls else None), c.name, sep=", ")
            if c.name == (cls.name if type(cls) in [_class, classes] else cls if type(
                    cls) == str else cls["name"] if type(cls) == dict and "name" in cls else None):
                c.level += lvl
                return c

    character_info: object = None

    Speed: dict[str, int] = None

    Hit_Points: object = None

    @property
    def Armor_Class(self):
        eq = self.Equipment
        ac = sum([e.ac for e in eq if hasattr(e, "ac")])
        return ac

    @property
    def Init(self): return mod(self.abilities[abilities.dex])

    _abilities = {
        abilities.STR: 0,
        abilities.DEX: 0,
        abilities.CON: 0,
        abilities.INT: 0,
        abilities.WIS: 0,
        abilities.CHA: 0
    }

    @property
    def Abilities(self):
        t = copy_dict(self._abilities)

        asis = []
        for c in self.Classes:
            if c == None:
                continue
            ts = c.traits
            for i in range(1, c.level+1):
                i = str(i)
                if not i in ts:
                    continue
                for tr in ts[i]:
                    if tr.ASI != None:
                        asis.append(tr.ASI)

        for tr in self.Race.value.traits:
            if tr.ASI != None:
                asis.append(tr.ASI)

        for k in t.keys():
            for asi in asis:
                if k in asi:
                    t[k] += asi[k]
        return t

    _skills: dict[skills, bool] = {
        skills.Acrobatics: False,
        skills.Animal: False,
        skills.Arcana: False,
        skills.Athletics: False,
        skills.Deception: False,
        skills.History: False,
        skills.Insight: False,
        skills.Intimidation: False,
        skills.Investigation: False,
        skills.Medicine: False,
        skills.Nature: False,
        skills.Perception: False,
        skills.Performance: False,
        skills.Persuasion: False,
        skills.Religion: False,
        skills.Sleight: False,
        skills.Stealth: False,
        skills.Survival: False,
    }

    @property
    def Skills(self):
        profs = {k: 1 if v else 0 for k, v in self._skills.items()}

        for t in self.Race.value.traits:
            if t.prof != None:
                for k, v in t.prof.items():
                    profs[k] += 1 if type(v) == bool and v else v if type(v) in [
                        int, float] else 0

        for c in self.Classes:
            if c == None:
                continue
            c: _class = c.value
            ts = c.traits
            for i in range(1, c.level+1):
                if not i in ts:
                    continue
                for t in ts[i]:
                    if t.prof != None:
                        for k, v in t.prof.items():
                            profs[k] += 1 if type(v) == bool and v else v if type(v) in [
                                int, float] else 0

        return profs

    @property
    def Saving_Throws(self):
        r = []
        for cls, lvl in self.Classes:
            t: _class = cls.value
            r += t.saving_throw_profs
        r = clear_dup(r)
        return r

    @property
    def Weapon_Proficiencies(self):
        r = []
        for cls, lvl in self.Classes:
            t: _class = cls.value
            r += t.armor_weapon_profs
        r = clear_dup(r)
        return r

    Inventory: list[_item] = []

    @property
    def Equipment(self):  # need item pointer instead
        return [i for i in self.Inventory if type(i) in [_weapon, _armor]]


p = Player()
p.Classes = classes.Barbarian
p.Race = races.hill_dwarf


log(p.Abilities)
c = p.level_up_class(classes.Barbarian)

log(p.Abilities)
