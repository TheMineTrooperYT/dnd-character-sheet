#!/usr/bin/python

from __future__ import print_function
import sys


# prints  a red error to STDERR


def eprint(*args, **kwargs):
    print(
        bcolors.FAIL,
        bcolors.BOLD,
        *args,
        bcolors.ENDC,
        file=sys.stderr,
        **kwargs
    )  # prepend red&bold codes to the msg, and append ENDC code to the end of it


# prints args with given colors list (or item)
def printc(colors, *args):
    t = colors if type(colors) == list else [
        colors
    ]  # if colors is a list, save it, else create a list with it
    t += list(args)  # add args as a list
    t.append(bcolors.ENDC)  # add color reset code
    print(*t)  # print


log = print  # lambda *items: print(*items)  # log function
elog = eprint  # lambda *items: eprint(*items)  # error log function


# color codes
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


#log = lambda *x: None
